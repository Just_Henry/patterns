package Strategy;

public class Runner {

    public static void main(String[] args) {
        Human human = new Human();

        human.setWrite(new French());
        human.executeWrite();

        human.setWrite(new Englishman());
        human.executeWrite();

        human.setWrite(new Russian());
        human.executeWrite();
    }
}
