package Strategy;

public class Human {
    Write write;

    public void setWrite(Write write){
        this.write = write;
    }

    public void executeWrite(){
        write.write();
    }
}
