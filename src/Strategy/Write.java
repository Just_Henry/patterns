package Strategy;

public interface Write {
    void write();
}
