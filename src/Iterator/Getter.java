package Iterator;

public interface Getter {
    Iterator getIterator();
}
