package Iterator;

public class Runner {
    public static void main(String[] args) {
        String[] skills = new String[]{"first skill","second skill","third skill"};
        Champion champion = new Champion("Ronin",skills);

        Iterator iterator = champion.getIterator();

        System.out.println("Hero "+champion.getName());
        System.out.println("Skills ");


        while(iterator.hasNext()){
            System.out.println(iterator.next().toString()+" ");
        }
    }
}
