package Bridge;

public class Archer extends Warrior{
    public Archer(WhatYouGot whatYouGot) {
        super(whatYouGot);
    }

    @Override
    public void warriorMaker() {
        System.out.println("I'm ranger");
        whatYouGot.show();
    }
}
