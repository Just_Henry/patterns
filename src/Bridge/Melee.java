package Bridge;

public class Melee extends Warrior{

    public Melee(WhatYouGot whatYouGot) {
        super(whatYouGot);
    }

    @Override
    public void warriorMaker() {
        System.out.println("I'm melee warrior");
        whatYouGot.show();
    }
}
