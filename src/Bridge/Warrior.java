package Bridge;

public abstract class Warrior {
    protected WhatYouGot whatYouGot;

    public Warrior(WhatYouGot whatYouGot) {
        this.whatYouGot = whatYouGot;
    }

    public abstract void warriorMaker();
}
