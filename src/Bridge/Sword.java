package Bridge;

public class Sword implements WhatYouGot{
    @Override
    public void show() {
        System.out.println("I've got sword");
    }
}
