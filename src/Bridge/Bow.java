package Bridge;

public class Bow implements WhatYouGot {
    @Override
    public void show() {
        System.out.println("I've got the bow");
    }
}
