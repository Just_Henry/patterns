package Bridge;

public class CreatePerson {
    public static void main(String[] args) {
        Warrior[] warriors ={
                new Archer(new Bow()),
                new Melee(new Sword())
        };

        for(Warrior warrior: warriors){
            warrior.warriorMaker();
        }
    }
}
