package TemplateMethod;

public class John extends HumanTemplate{

    public void Performance(){
        System.out.println("Hello!");
        System.out.println("My name is John");
        System.out.println("Bye!");
    }

    @Override
    public String tellName() {
        return "My name is John";
    }
}
