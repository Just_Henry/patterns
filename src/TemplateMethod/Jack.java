package TemplateMethod;

public class Jack extends HumanTemplate{
    public void Performance(){
        System.out.println("Hello!");
        System.out.println("My name is Jack");
        System.out.println("Bye!");
    }

    @Override
    public String tellName() {
       return "My name is Jack";
    }
}
