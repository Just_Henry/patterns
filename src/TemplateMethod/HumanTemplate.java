package TemplateMethod;

public abstract class HumanTemplate {
    public void Name(){
        System.out.println("Hello!");
        System.out.println(tellName());
        System.out.println("Bye!");
    }

    public abstract String tellName();
}
