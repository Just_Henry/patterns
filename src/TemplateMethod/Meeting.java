package TemplateMethod;

public class Meeting {
    public static void main(String[] args) {
        HumanTemplate humanTemplate1 = new Barry();
        HumanTemplate humanTemplate2 = new Jack();
        HumanTemplate humanTemplate3 = new John();


        humanTemplate1.Name();
        humanTemplate2.Name();
        humanTemplate3.Name();
    }

}
