package AbstractFabric.Interfaces;

public interface Team {
    Melee getMelee();
    Ranged getRanged();
}
