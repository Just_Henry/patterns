package AbstractFabric;


import AbstractFabric.Enemies.EnemyTeam;
import AbstractFabric.Heroes.HeroesTeam;
import AbstractFabric.Interfaces.Melee;
import AbstractFabric.Interfaces.Ranged;
import AbstractFabric.Interfaces.Team;

public class Fight {

    public static void main(String[] args) {
        Team team1 = new HeroesTeam();
        Ranged archer = team1.getRanged();
        Melee warrior = team1.getMelee();
        Team team2 = new EnemyTeam();
        Ranged dragon = team2.getRanged();
        Melee orc = team2.getMelee();

        System.out.println("Ready? Fight!");
        System.out.println("Round 1");
        System.out.println("Show me what you got");
        archer.showStuff();
        dragon.showStuff();
        System.out.println("Enemies team won!");

        System.out.println("Round 2");
        System.out.println("Show me what you got");
        warrior.showStuff();
        orc.showStuff();
        System.out.println("Heroes team won!");
    }////////////

}
