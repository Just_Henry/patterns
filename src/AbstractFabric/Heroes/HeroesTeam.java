package AbstractFabric.Heroes;

import AbstractFabric.Interfaces.Melee;
import AbstractFabric.Interfaces.Ranged;
import AbstractFabric.Interfaces.Team;

public class HeroesTeam implements Team {

    @Override
    public Melee getMelee() {
        return new Warrior();
    }

    @Override
    public Ranged getRanged() {
        return new Archer();
    }
}
