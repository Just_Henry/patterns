package AbstractFabric.Enemies;


import AbstractFabric.Interfaces.Melee;
import AbstractFabric.Interfaces.Ranged;
import AbstractFabric.Interfaces.Team;

public class EnemyTeam implements Team {
    @Override
    public Melee getMelee() {
        return new Orc();
    }

    @Override
    public Ranged getRanged() {
        return new Dragon();
    }
}
