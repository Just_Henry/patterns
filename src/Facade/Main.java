package Facade;

public class Main {
    public static void main(String[] args) {
        FacadePerson facadePerson = new FacadePerson();
        facadePerson.createPerson();
    }
}
