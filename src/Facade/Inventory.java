package Facade;

public class Inventory {
    private String money;
    private String weapon;

    public Inventory() {
        this.weapon = "no";
        this.money = "0";
    }

    public Inventory(String money, String weapon) {
        this.money = money;
        this.weapon = weapon;
    }

    public void show(){
        System.out.println(money+" gold\n"+weapon+" weapon\n");
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "money='" + money + '\'' +
                ", weapon='" + weapon + '\'' +
                '}';
    }
}
