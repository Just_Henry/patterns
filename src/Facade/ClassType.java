package Facade;

public class ClassType {
    private Boolean type;

    public Boolean getType() {
        return type;
    }

    public void classMaker(Boolean type){
        if(type == true){
            System.out.println("Class is Warrior\n");
        }else System.out.println("Class is archer\n");
    }

    @Override
    public String toString() {
        return "ClassType{" +
                "type=" + type +
                '}';
    }
}
