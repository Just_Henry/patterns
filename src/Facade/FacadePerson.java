package Facade;

public class FacadePerson {
    ClassType classType = new ClassType();
    Inventory inventory = new Inventory();
    Attributes attributes = new Attributes(10,10);


    public void createPerson(){
        classType.classMaker(true);
        inventory.show();
        attributes.showAttributes();
    }

    @Override
    public String toString() {
        return "FacadePerson{" +
                "classType=" + classType +
                ", inventory=" + inventory +
                ", attributes=" + attributes +
                '}';
    }
}
