package Facade;

public class Attributes {
    private int strength;
    private int dexterity;

    public Attributes() {
        this.strength = 0;
        this.dexterity = 0;
    }

    public Attributes(int strength, int dexterity) {
        this.strength = strength;
        this.dexterity = dexterity;
    }

    public void showAttributes(){
        System.out.println(strength+" - strength\n"+dexterity+" - dexterity\n");
    }

    @Override
    public String toString() {
        return "Attributes{" +
                "strength=" + strength +
                ", dexterity=" + dexterity +
                '}';
    }
}
