package State;

public class Runner {
    public static void main(String[] args) {
        Activity activity = new Sleeping();
        Human human = new Human();

        human.setActivity(activity);

        for(int i = 0; i< 10; i++){
            human.action();
            human.changeActivity();
        }
    }
}
