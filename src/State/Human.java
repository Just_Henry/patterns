package State;

public class Human {
    Activity activity;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void changeActivity(){
        if(activity instanceof Sleeping){
            setActivity(new Eating());
        }else if(activity instanceof Eating){
            setActivity(new Walking());
        }else if(activity instanceof Walking){
            setActivity(new Sleeping());
        }
    }

    public void action(){
        activity.action();
    }
}
