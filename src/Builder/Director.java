package Builder;

public class Director {
    ComputerBuilder computerBuilder;

    public void setComputerBuilder(ComputerBuilder computerBuilder) {
        this.computerBuilder = computerBuilder;
    }

    Computer builder(){
        computerBuilder.cpuBuilder();
        computerBuilder.gpuBuilder();;
        computerBuilder.ramBuilder();;
        computerBuilder.accessoriesBuilder();

        Computer computer = computerBuilder.getComputer();
        return computer;
    }
}
