package Builder;

public class SuperComputer extends ComputerBuilder{

    @Override
    void gpuBuilder() {
        System.out.println("GPU - Nvidia 3090");
    }

    @Override
    void cpuBuilder() {
        System.out.println("CPU - i9 10900k");
    }

    @Override
    void ramBuilder() {
        System.out.println("RAM - 2400 GH");
    }

    @Override
    void accessoriesBuilder() {
        System.out.println(Accessories.HEADPHONES+" - HyperX \n"+Accessories.VR+" is included\n"+Accessories.KEYBOARD+" - Razer\n"+Accessories.MOUSE+" - SteelSeries\n");
    }
}
