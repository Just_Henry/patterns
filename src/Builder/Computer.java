package Builder;

public class Computer {
    private String gpu;
    private String cpu;
    private String price;
    Accessories accessories;

    public void setGpu(String gpu) {
        this.gpu = gpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setAccessories(Accessories accessories) {
        this.accessories = accessories;
    }

    public String getGpu() {
        return gpu;
    }

    public String getCpu() {
        return cpu;
    }

    public String getPrice() {
        return price;
    }

    public Accessories getAccessories() {
        return accessories;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "gpu='" + gpu + '\'' +
                ", cpu='" + cpu + '\'' +
                ", price='" + price + '\'' +
                ", accessories=" + accessories +
                '}';
    }
}
