package Builder;

public class Client {
    public static void main(String[] args) {
        Director director = new Director();

        director.setComputerBuilder(new SuperComputer());
        Computer computer = director.builder();

        System.out.println(computer);
    }
}
