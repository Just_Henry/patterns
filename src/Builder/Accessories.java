package Builder;

public enum Accessories {
    MOUSE, KEYBOARD, HEADPHONES, VR;
}
