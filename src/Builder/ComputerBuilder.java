package Builder;

public abstract class ComputerBuilder {
    Computer computer;

    void createComputer(){
        computer = new Computer();
    }

    abstract void gpuBuilder();
    abstract void cpuBuilder();
    abstract void ramBuilder();
    abstract void accessoriesBuilder();

    public Computer getComputer() {
        return computer;
    }
}
