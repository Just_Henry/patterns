package Builder;

public class CheapComputer extends ComputerBuilder{
    @Override
    void gpuBuilder() {
        System.out.println("GPU - Nvidia 1650SUPER");
    }

    @Override
    void cpuBuilder() {
        System.out.println("CPU - Ryzen 5 2600");
    }

    @Override
    void ramBuilder() {
        System.out.println("RAM - 2100 GH");
    }

    @Override
    void accessoriesBuilder() {
        System.out.println(Accessories.MOUSE+" - Bloody\n"+Accessories.KEYBOARD+" - X7");
    }
}
