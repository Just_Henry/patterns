package Singletone;

public class Meeting {

    public static void main(String[] args) {
        Human.getHuman().addComment("He's nice guy...\n");
        Human.getHuman().addComment("He's very strong...\n");
        Human.getHuman().addComment("He loves sun...\n");
        Human.getHuman().addComment("He can praise it...\n");


        Human.getHuman().whatIsYourName();
        Human.getHuman().howOldAreYou();
        Human.getHuman().tellUsAboutYou();
    }
}
