package Singletone;

public class Human {
    private static Human human;
    private static String name = "Solaire";
    private static String character= "He's human...\n";
    private static int age = 30;

    private Human(){}

    public static Human getHuman(){
        if(human == null){
            human = new Human();
        }
        return human;
    }

    public void addComment(String comment){
        character += comment;
    }

    public void howOldAreYou() {
        System.out.println(age);
    }

    public void whatIsYourName(){
        System.out.println(name);
    }

    public void tellUsAboutYou(){
        System.out.println(character);
    }
}
