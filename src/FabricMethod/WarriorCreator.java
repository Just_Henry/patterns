package FabricMethod;

public class WarriorCreator implements CreateHero {
    @Override
    public Classes createHero() {
        return new Warrior();
    }
}
