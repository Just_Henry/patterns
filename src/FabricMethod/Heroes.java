package FabricMethod;

public class Heroes {
    public static void main(String[] args) {
        CreateHero archer = createHeroByClass("Archer");
        Classes archerHero = archer.createHero();

        archerHero.showStuff();
    }

    private static CreateHero createHeroByClass(String heroStuff) {
        if(heroStuff.equalsIgnoreCase("Archer")){
            return new ArcherCreator();
        }else if(heroStuff.equalsIgnoreCase("Warrior")){
            return new WarriorCreator();
        }else if(heroStuff.equalsIgnoreCase("Mage")){
            return new MageCreator();
        }else throw new IllegalArgumentException( heroStuff + "is npt a hero class");
    }
}
