package FabricMethod;

public class ArcherCreator implements CreateHero {
    @Override
    public Classes createHero() {
        return new Archer();
    }
}
