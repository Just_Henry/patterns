package Adaptor;

public class Person {
    public static void main(String[] args) {
        Movement person = new Mover();

        person.moveUp();
        person.moveDown();
        person.moveRight();
        person.moveLeft();
    }
}
