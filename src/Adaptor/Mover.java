package Adaptor;

public class Mover extends Runner implements  Movement{
    @Override
    public void moveUp() {
        runUp();
    }

    @Override
    public void moveDown() {
        runDown();
    }

    @Override
    public void moveRight() {
        runRight();
    }

    @Override
    public void moveLeft() {
        runLeft();
    }
}
