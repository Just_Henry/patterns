package Adaptor;

public class Runner {
    public int x = 0;
    public int y = 0;

    public void runUp(){
        y+=100;
        System.out.println(x+" - x, "+y+" - y");
    }

    public void runDown(){
        y-=100;
        System.out.println(x+" - x, "+y+" - y");
    }

    public void runRight(){
        x+=100;
        System.out.println(x+" - x, "+y+" - y");
    }

    public void runLeft(){
        x-=100;
        System.out.println(x+" - x, "+y+" - y");
    }
}
