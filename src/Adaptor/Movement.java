package Adaptor;

public interface Movement {
    void moveUp();
    void moveDown();
    void moveRight();
    void moveLeft();
}
