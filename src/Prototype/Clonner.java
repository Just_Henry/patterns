package Prototype;

public class Clonner {

    public static void main(String[] args) {
        Project first = new Project('1', "SuperProject", "Src");


        System.out.println(first);

        ProjectFactory factory = new ProjectFactory(first);
        Project firstOne = factory.cloneProject();
        System.out.println("\n_______________________________\n");

        System.out.println(firstOne);

    }
}
