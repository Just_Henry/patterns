package Prototype;

public class Project implements Copyable{
    private char number;
    private String projectName;
    private String srcCode;

    public Project(char number, String projectName, String srcCode) {
        this.number = number;
        this.projectName = projectName;
        this.srcCode = srcCode;
    }

    public void setNumber(char number) {
        this.number = number;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setSrcCode(String srcCode) {
        this.srcCode = srcCode;
    }

    public char getNumber() {
        return number;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getSrcCode() {
        return srcCode;
    }

    @Override
    public String toString() {
        return "Project{" +
                "number=" + number +
                ", projectName='" + projectName + '\'' +
                ", srcCode='" + srcCode + '\'' +
                '}';
    }

    @Override
    public Object copy() {
        Project project = new Project(number, projectName, srcCode);
        return project;
    }
}
