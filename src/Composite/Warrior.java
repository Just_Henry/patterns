package Composite;

public class Warrior implements Fighter {
    @Override
    public String toString() {
        return "Warrior{}";
    }

    @Override
    public void tell() {
        System.out.println("Warrior\n");
    }
}
