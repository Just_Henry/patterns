package Composite;

public class Fight {
    public static void main(String[] args) {


        Team team = new Team();

        Fighter archer = new Archer();
        Fighter warrior = new Warrior();

        team.add(archer);
        team.add(warrior);

        System.out.println(team);
        team.show();
    }
}
