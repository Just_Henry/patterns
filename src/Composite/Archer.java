package Composite;

public class Archer implements Fighter {
    @Override
    public void tell() {
        System.out.println("Archer\n");
    }

    @Override
    public String toString() {
        return "Archer{}";
    }
}
