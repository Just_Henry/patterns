package Composite;

import java.util.ArrayList;
import java.util.List;

public class Team {
   private List<Fighter>list = new ArrayList<Fighter>();

    public Team(ArrayList<Fighter> list) {
        this.list = list;
    }

    public Team() {
    }

    public void add(Fighter fighter) {
        list.add(fighter);
    }

    public void show(){
        System.out.println("Creating team\n");
        for(Fighter fighter: list){
            fighter.tell();
        }
    }

    @Override
    public String toString() {
        return "Team{" +
                "list=" + list +
                '}';
    }
}
