package Composite;

public interface Fighter {
    public void tell();
}
