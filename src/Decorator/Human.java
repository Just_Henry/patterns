package Decorator;

public interface Human {
    String whoAreYou();
}
