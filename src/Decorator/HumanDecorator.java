package Decorator;

public class HumanDecorator implements Human{
    private Human human;

    public HumanDecorator(Human human) {

        this.human = human;
    }

    @Override
    public String whoAreYou() {
        return human.whoAreYou();
    }

    @Override
    public String toString() {
        return "HumanDecorator{" +
                "human=" + human +
                '}';
    }
}
