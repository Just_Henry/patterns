package Decorator;

public class Creator {

    public static void main(String[] args) {
        Human human = new HumanDecorator(new Man());

        System.out.println(human.whoAreYou());
    }
}
