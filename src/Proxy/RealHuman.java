package Proxy;

public class RealHuman implements Human{
    private String gender;

    public RealHuman(String gender) {
        this.gender = gender;
        Gender();
    }

    public void Gender(){
        System.out.println("i'm "+gender);
    }

    @Override
    public void tell() {
        System.out.println(gender);
    }
}
