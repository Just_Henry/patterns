package Proxy;

public class main {
    public static void main(String[] args) {
        Human human = new RealHuman("Man");

        human.tell();
    }
}
