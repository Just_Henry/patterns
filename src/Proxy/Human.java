package Proxy;

public interface Human {
    void tell();
}
